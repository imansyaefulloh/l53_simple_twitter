<?php

namespace App\Http\Controllers;

use App\Post;
use Illuminate\Http\Request;

class PostController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request, Post $post)
    {
        $followings = $request->user()->following()->pluck('users.id');

        $posts = $post->with('user')
            ->whereIn('user_id', $followings->push($request->user()->id))
            ->orderBy('created_at', 'desc')
            ->take($request->get('limit', 10))
            ->get();

        return response()->json([
            'posts' => $posts,
            'total' => $post->count()
        ]);
    }

    public function create(Request $request, Post $post)
    {
        $this->validate($request, [
            'body' => 'required|max:140'
        ]);

        $createdPost = $request->user()->posts()->create([
            'body' => $request->body
        ]);

        $post = $post->with('user')->find($createdPost->id);

        return response()->json($post);
    }
}
